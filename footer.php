<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Oscar Batlle
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'oscar-batlle' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'oscar-batlle' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'oscar-batlle' ), 'oscar-batlle', '<a href="http://oscarbatlle.com" rel="designer">Oscar Batlle</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<!-- bower:js -->
<!-- endbower -->

<!-- inject:js -->
<!-- endinject -->

<?php wp_footer(); ?>

</body>
</html>
