// Include gulp
var gulp = require('gulp'),

// Define base folders
src      = 'assets/',
dest     = 'public/',

// Define bower folders
  config = {
    sassPath: './assets/scss',
    bowerDir: './bower_components',
},

// Include plugins
sass     = require('gulp-ruby-sass'),
notify   = require("gulp-notify"),
bower    = require('gulp-bower'),
concat   = require('gulp-concat'),
uglify   = require('gulp-uglify'),
rename   = require('gulp-rename'),
imagemin = require('gulp-imagemin'),
cache    = require('gulp-cache'),
wiredep  = require('wiredep').stream,
bs       = require('browser-sync').create();

// Minify CSS using Sass
gulp.task('sass', function(){
    return sass(src + 'scss/app.scss', {style: 'compressed'})
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest(dest + 'css'))
  .pipe(bs.stream());
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(src + 'js/*.js')
      .pipe(concat('main.js'))
	.pipe(rename({suffix: '.min'}))
	.pipe(uglify())
        .pipe(gulp.dest(dest + 'js'))
        .pipe(bs.stream());
});

// Image Optimisation
gulp.task('images', function(){
    return gulp.src(src + 'images/**/*')
	.pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
	.pipe(gulp.dest(dest + 'images'))
  .pipe(bs.stream());
});

// Wiredep support to Bower packages
gulp.task('wiredep', function () {
  // Add JS files
  gulp.src('./footer.php')
    .pipe(wiredep())
    .pipe(gulp.dest('./'));
  // Add CSS files
  gulp.src('./header.php')
    .pipe(wiredep())
    .pipe(gulp.dest('./'));
});

// Move fontawesome to public/fonts
gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*')
        .pipe(gulp.dest('./public/fonts'));
});

// browser-sync Static server
gulp.task('browser-sync', function() {
  files: ['*.php','public/css/*.css', 'public/js/*.js'],
    bs.init({
        server: {
            baseDir: "./"
        }
    });
});

// Watch for changes
gulp.task('watch', function(){
  // Watch .js files
  gulp.watch(src + 'js/*.js', ['scripts']);
   // Watch .scss files
  gulp.watch(src + 'scss/*.scss', ['sass']);
   // Watch image files
  gulp.watch(src + 'images/**/*', ['images']);
});

// Default Task
gulp.task('default', ['bower', 'scripts', 'sass', 'images', 'watch']);
